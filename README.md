# ics-ans-jmasar

Ansible playbook to install [jmasar-service](https://gitlab.esss.lu.se/ics-software/jmasar-service).

## Requirements

- ansible >= 2.4
- molecule >= 2.6

## License

BSD 2-clause
